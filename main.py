#!/usr/bin/env python
# -*- coding: utf-8 -*-
from mower import Mower
from lawn import Lawn
from log import Log
import sys
import argparse

logger = Log().logger


class Loader:
    def __init__(self):
        parser = argparse.ArgumentParser(prog=sys.argv[0])
        parser.add_argument('-f', '--config', nargs='?', help="configuration file to use", default="mowers.conf")
        args = parser.parse_args()
        self.config = args.config
        self.lawn = None
        self.mowers = []
        self.read_configuration()

    def read_configuration(self):
        try:
            with open(self.config, "r") as conf:
                lawn_init = conf.readline()
                self.lawn = Lawn(lawn_init)
                while True:
                    mower_init = conf.readline().rstrip()
                    mower_directions = conf.readline().rstrip()
                    if not mower_directions:
                        if mower_init:
                            logger.error("incomplete mower definition, aborting.")
                            sys.exit(2)
                        logger.info("Finished loading mowers")
                        break
                    self.mowers.append(Mower(mower_init, mower_directions, len(self.mowers), self.lawn))

        except EnvironmentError:
            logger.error("Could not find config file [{}], aborting.".format(self.config))
            sys.exit(1)


if __name__ == '__main__':
    loader = Loader()
    lawn = loader.lawn
    lawn.maw()


