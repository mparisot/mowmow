# -*- coding: utf-8 -*-
from log import Log
logger = Log().logger


class Mower:
    """ Enterprise grade mower
        ----------------------
        x: position on the X axis
        y: position on the Y axis
        orientation: direction in which the mower will move
        speed: number of movements per move
        id: unique identifier of the mower
    """

    def __init__(self, initial_position, directions, mower_id, lawn, speed=1):
        self.x = None
        self.y = None
        self.orientation = None
        self.moves = None
        self.speed = speed
        self.id = mower_id
        self.lawn = lawn
        self.validate_initial_position(initial_position)
        self.validate_moves(directions)
        self.dump("New", show_moves=True)

    def dump(self, prefix="Current", show_moves=False, debug=False):
        method = logger.info
        if debug:
            method = logger.debug

        moves = ""
        if show_moves:
            moves = " moves={}".format(self.moves)
        method("{} mower:  id=[{}] x=[{}] y=[{}] orientation=[{}]{}".format(
            prefix,
            self.id,
            self.x,
            self.y,
            self.orientation,
            moves
        ))

    def validate_initial_position(self, initial_position):
        try:
            x, y, orientation = initial_position.split()
            x = int(x)
            y = int(y)
            orientation = self.validate_orientation(orientation)
            self.x, self.y = self.lawn.place(self, x, y, initial_position=True)
            self.orientation = orientation
        except ValueError as e:
            logger.error("Invalid mower initialization: [{}], mower_id=[{}]".format(initial_position, self.id))
            raise e

    def validate_moves(self, moves):
        ''.join(map(self.validate_move, moves))
        self.moves = list(moves)

    @staticmethod
    def validate_orientation(orientation):
        logger.debug("validating orientation [{}]".format(orientation))
        if orientation not in ['N', 'S', 'E', 'W']:
            logger.error("Invalid orientation: [{}]".format(orientation))
            raise ValueError
        return orientation

    @staticmethod
    def validate_move(move):
        logger.debug("validating move [{}]".format(move))
        if move not in ['L', 'R', 'F']:
            logger.error("Invalid move: [{}]".format(move))
            raise ValueError
        return move

    def change_orientation(self, move):
        if move == 'F':
            return self.orientation
        if move == 'L':
            directions = ['N', 'E', 'S', 'W']
        elif move == 'R':
            directions = ['W', 'S', 'E', 'N']
        else:
            logger.error("Invalid orientation [{}], something is badly broken, aborting.".format(self.orientation))
            raise ValueError

        return directions[directions.index(self.orientation) - 1]

    def maw(self):
        self.dump()
        for move in self.moves:
            logger.debug("Handling action [{}] for mower [{}]".format(move, self.id))
            self.orientation = self.change_orientation(move)
            self.dump(debug=True)
            if move != 'F':
                logger.debug("Current action=[{}], not moving".format(move))
                continue
            else:
                logger.debug("Moving forward to [{}]".format(self.orientation))
            if self.orientation == 'N':
                logger.debug("moving to the north")
                self.x, self.y = self.lawn.place(self, self.x, min(self.y + 1, self.lawn.height - 1))
            elif self.orientation == 'E':
                logger.debug("moving to the east")
                self.x, self.y = self.lawn.place(self, min(self.x + 1, self.lawn.width - 1), self.y)
            elif self.orientation == 'S':
                logger.debug("moving to the south")
                self.x, self.y = self.lawn.place(self, self.x, max(self.y - 1, 0))
            elif self.orientation == 'W':
                logger.debug("moving to the west")
                self.x, self.y = self.lawn.place(self, max(self.x - 1, 0), self.y)
            else:
                logger.error("Invalid move [{}], something is badly broken. Aborting.".format(move))
                raise ValueError
        logger.info("Final position for mower[{}]: x=[{}] y=[{}] orientation=[{}]".format(self.id,
                                                                                          self.x,
                                                                                          self.y,
                                                                                          self.orientation))




