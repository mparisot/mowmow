# -*- coding: utf-8 -*-
import logging
import sys

logLevel = logging.INFO

class Log:
    def __init__(self):
        logger = logging.getLogger()
        if logger.handlers:
            logger.handlers = []
        else:
            log_handler = logging.StreamHandler(sys.stdout)
            formatter = logging.Formatter('%(asctime)s - [%(levelname)s] - [%(module)s/%(funcName)s:%(lineno)d] - %(message)s')
            log_handler.setFormatter(formatter)
            logger.addHandler(log_handler)
            logger.setLevel(logLevel)
        self.logger = logger
