#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
from mower import Mower
from lawn import Lawn
from log import Log
logger = Log().logger


class MowerTests(unittest.TestCase):
    def setUp(self):
        logger.info("Test {} Started".format(self._testMethodName))

    def tearDown(self):
        logger.info("Test {} is finished\n\n".format(self._testMethodName))

    def test_change_orientation(self):
        lawn = Lawn("8 8")
        mower = Mower("3 2 E", "FFLFRFF", 0, lawn)
        self.assertEqual(mower.change_orientation("L"), "N")
        mower.orientation = "N"
        self.assertEqual(mower.change_orientation("L"), "W")
        mower.orientation = "W"
        self.assertEqual(mower.change_orientation("L"), "S")
        mower.orientation = "S"
        self.assertEqual(mower.change_orientation("L"), "E")
        self.assertEqual(mower.change_orientation("R"), "W")
        mower.orientation = "W"
        self.assertEqual(mower.change_orientation("R"), "N")
        mower.orientation = "N"
        self.assertEqual(mower.change_orientation("R"), "E")
        mower.orientation = "E"
        self.assertEqual(mower.change_orientation("R"), "S")

    def test_maw_examples(self):
        lawn = Lawn("5 5")
        mower = Mower("1 2 N", "LFLFLFLFF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 1)
        self.assertEqual(mower.y, 3)
        self.assertEqual(mower.orientation, "N")
        mower = Mower("3 3 E", "FFRFFRFRRF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 5)
        self.assertEqual(mower.y, 1)
        self.assertEqual(mower.orientation, "E")

    def test_maw_blocking(self):
        lawn = Lawn("5 5")
        mower1 = Mower("1 2 N", "FFLFFFLFFFFLFFFFF", 0, lawn)
        mower2 = Mower("0 2 N", "L", 0, lawn)
        mower3 = Mower("1 3 N", "L", 0, lawn)
        mower4 = Mower("1 1 N", "L", 0, lawn)
        mower5 = Mower("2 2 N", "L", 0, lawn)
        mower1.maw()
        self.assertEqual(mower1.x, 1)
        self.assertEqual(mower1.y, 2)
        self.assertEqual(mower1.orientation, "E")

    def test_upper_wall_blocking(self):
        lawn = Lawn("5 5")
        mower = Mower("4 4 N", "FFFF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 4)
        self.assertEqual(mower.y, 5)
        self.assertEqual(mower.orientation, "N")

    def test_lower_wall_blocking(self):
        lawn = Lawn("5 5")
        mower = Mower("3 2 S", "FFFF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 3)
        self.assertEqual(mower.y, 0)
        self.assertEqual(mower.orientation, "S")

    def test_left_wall_blocking(self):
        lawn = Lawn("5 5")
        mower = Mower("1 0 W", "FFFF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 0)
        self.assertEqual(mower.y, 0)
        self.assertEqual(mower.orientation, "W")

    def test_right_wall_blocking(self):
        lawn = Lawn("5 5")
        mower = Mower("4 2 E", "FFFF", 0, lawn)
        mower.maw()
        self.assertEqual(mower.x, 5)
        self.assertEqual(mower.y, 2)
        self.assertEqual(mower.orientation, "E")

    def test_two_mowers_one_spot_at_initialization(self):
        lawn = Lawn("5 5")
        mower1 = Mower("1 2 N", "FFFF", 0, lawn)
        self.assertRaises(ValueError, Mower, "1 2 N", "FFFF", 1, lawn)

    def test_mower_outside_lawn_at_initialization(self):
        lawn = Lawn("5 5")
        self.assertRaises(ValueError, Mower, "6 5 N", "FFFF", 0, lawn)
        self.assertRaises(ValueError, Mower, "-1 5 N", "FFFF", 0, lawn)

    def test_lawn_init(self):
        self.assertRaises(ValueError, Lawn, "Z blh")


if __name__ == '__main__':
    unittest.main()
