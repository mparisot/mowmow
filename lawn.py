# -*- coding: utf-8 -*-
from log import Log
logger = Log().logger


class Lawn:
    """ Area in which the mower will move
        ---------------------------------
        width: width of the lawn
        height: height of the lawn
        grid: rep
        mowers: a list of mowers
        capacity: number of allowed mowers per square

    """
    def __init__(self, lawn_init):
        self.width = 0
        self.height = 0
        self.capacity = 1
        self.mowers = []
        max_x, max_y = lawn_init.split()
        try:
            self.width = int(max_x) + 1
            self.height = int(max_y) + 1
        except ValueError as e:
            logger.error("Invalid grid initialization values: max_x=[{}] max_y=[{}]. Aborting.".format(max_x,
                                                                                                       max_y))
            raise e
        self.grid = [[] for _ in range(self.width * self.height)]
        logger.info("Initialized lawn: width=[{}] height=[{}]".format(self.width, self.height))

    def place(self, mower, x, y, initial_position=False):
        logger.debug("Trying to place mower[{}]".format(mower.id))
        new_x, new_y = self.validate_coordinates(mower, x, y, initial_position)
        if initial_position:
            self.grid[self.coordinates_to_position(new_x, new_y)].append(mower)
            self.mowers.append(mower)
        else:
            pos = self.coordinates_to_position(mower.x, mower.y)
            self.grid[pos].remove(mower)
            pos = self.coordinates_to_position(new_x, new_y)
            self.grid[pos].insert(0, mower)
            logger.debug("Moved mower[{}] to x=[{}] y=[{}]".format(mower.id, new_x, new_y))
        return new_x, new_y

    def coordinates_to_position(self, x, y):
        return y * self.width + x

    def position_to_coordinates(self, position):
        x = position % self.width
        y = position / self.width
        return x, y

    """ Validate target coordinates before moving the mower
        If it's an initial placement, crash if spot capacity is exceeded.
        If it's not an initial placement, stay where you are if the spot is full        
    """
    def validate_coordinates(self, mower, x, y, initial_position):
        if x >= self.width or y >= self.height or x < 0 or y < 0 and initial_position:
            logger.error("Out of range coordinates: x=[{}] width=[{}] y=[{}] height=[{}], aborting.".format(x,
                                                                                                            self.width,
                                                                                                            y,
                                                                                                            self.height)
                         )
            raise ValueError
        position = self.coordinates_to_position(x, y)
        if len(self.grid[position]) >= self.capacity:
            if initial_position:
                logger.error("No more space on spot x=[{}] y=[{}], aborting.".format(x, y))
                raise ValueError
            return mower.x, mower.y
        return x, y

    def maw(self):
        logger.info("Starting to mow with [{}] mower(s)".format(len(self.mowers)))
        for mower in self.mowers:
            mower.maw()

