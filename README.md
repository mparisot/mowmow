Automatic mowers exercise
=====

Developed with Intellij PyCharm, runs on Python 2 and Python 3

## Setup:
First, create a virtualenv, activate it and install requirements:
```bash
$ virtualenv .venv
$ source .venv/bin/activate
$ pip install -f requirements.txt
```

## Run it:
```bash
$ ./main.py
# or with an alternative configuration file:
$ ./main.py -f /tmp/config.txt
```

## Run tests:
```bash
$ ./mower_tests.py
```
